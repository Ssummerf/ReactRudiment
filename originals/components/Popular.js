var React = require('react');
var Link = require('react-router-dom').Link;

class Popular extends React.Component {
  render() {
    return (
      <div className='home-container'>
        <h1>This is Scott Summerfords demonstration for React Rudiment 1</h1>
        <h2>More code can be seen at gitlab.com @Ssummerf </h2>
      </div>
    )
  }
}

module.exports = Popular;