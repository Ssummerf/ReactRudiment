var React = require('react');
var Link = require('react-router-dom').Link;

class Home extends React.Component {
  render() {
    return (
      <div className='home-container'>
        <h1>Github Searching</h1>
        <img 
        src={'https://www.aha.io/assets/github.7433692cabbfa132f34adb034e7909fa.png'}
        alt="boohoo" 
        className="img-responsive"
        />
      </div>
    )
  }
}

module.exports = Home;