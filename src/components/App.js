import Upload from './Upload';
import Heatmap from './heat-map';
import Analysis from './Analysis';

var React = require('react');
var ReactRouter = require('react-router-dom');
var Router = ReactRouter.BrowserRouter;
var Route = ReactRouter.Route;
var Switch = ReactRouter.Switch;
var Nav = require('./Nav');
var Home = require('./Home');
var Login = require('./Login');
var Vectormap = require('./Vector-Map');
var MiceRedirect = require('./MiceRedirect');

class App extends React.Component {
  render() {
    return (
      <Router>
        <div className='container'>
          <Nav />

          <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/Heat-Map' component={Heatmap} />
            <Route exact path='/Vector-Map' component={Vectormap} />
            <Route exact path='/Login' component={Login} />
            <Route exact path='/Analysis' component={Analysis} />

            <Route path='/MiceRedirect' component={() => window.location = 'http://35.243.133.187/'}/>

            <Route exact path='/Upload' component={Upload} />

            <Route render={function () {
              return <p>Not Found</p>
            }} />
          </Switch>
        </div>
      </Router>
    )
  }
}

export default App;