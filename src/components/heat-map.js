import ReactFileReader from 'react-file-reader';
import PropTypes from 'prop-types';
import moment from 'moment';
import HeatMap from "react-heatmap-grid";
import Papa from 'papaparse';
import _ from 'lodash';
{/*------------------------IMPORTS / GLOBAL CONSTS------------------------------*/}
{/*general imports*/}
var React = require('react');
{/*imports for datetime input*/}
var Calendar = require('rc-calendar/lib');
var DatePicker = require('rc-calendar/lib/Picker');
var TimePickerPanel = require('rc-time-picker/lib/Panel');
{/*general format constants / imported CSS files*/}	
require('rc-calendar/assets/index.css');
require('rc-time-picker/assets/index.css');

{/*--------------------------------------------------------------*/}
{/*-----------------BELOW STARTS MAKING THE COMPONENT------------------------*/}

class Heatmap extends React.Component {

constructor (props) {
	super(props);
	this.state = {
	datapoints:[[],[]],
	csv:[],
	csvdata_mice_rows:[],
	time_start: moment("12-12-1970","YYYY-MM-DD"),
	time_end: moment(),
	x_labels: [],
	y_labels: [],
	mice_RED:0,
	mice_BLUE:0,
	mice_GREEN:0,
	mice_ORANGE:0,
	mice_RED2:0,
	mice_BLUE2:0,
	mice_GREEN2:0,
	mice_ORANGE2:0,
	file_select:"0",
	waiting: 'Waiting on Job',
    	processing: '',
    	complete: ''
};
this.onChange2 = this.onChange2.bind(this);//used for selecting mice
this.radioChange = this.radioChange.bind(this);//used for selecting CSV
this.quick = this.quick.bind(this);//used for making heatmap

}






//*-----------------functions for component------------

radioChange(event) {
  this.setState({
    file_select: event.target.value
  });
  this.setState({
    prev_file_select: event.target.value
  });
}




onChange2 (e){
//this.setState({value2,});
var checked = e.target.checked;
var button = e.target.name;
if(button=="mouse9"){if(checked){this.setState({mice_RED: 1,mice_BLUE: 1,mice_GREEN: 1,mice_ORANGE: 1,mice_RED2: 1,mice_BLUE2: 1,mice_GREEN2: 1,mice_ORANGE2: 1});}else{this.setState({mice_RED: 0,mice_BLUE: 0,mice_GREEN: 0,mice_ORANGE: 0,mice_RED2: 0,mice_BLUE2: 0,mice_GREEN2: 0,mice_ORANGE2: 0});}}
if(button=="mouse1"){if(checked){this.setState({mice_RED: 1});}else{this.setState({mice_RED: 0});}}
if(button=="mouse2"){if(checked){this.setState({mice_BLUE: 1});}else{this.setState({mice_BLUE: 0});}}
if(button=="mouse3"){if(checked){this.setState({mice_GREEN: 1});}else{this.setState({mice_GREEN: 0});}}
if(button=="mouse4"){if(checked){this.setState({mice_ORANGE: 1});}else{this.setState({mice_ORANGE: 0});}}
if(button=="mouse5"){if(checked){this.setState({mice_RED2: 1});}else{this.setState({mice_RED2: 0});}}
if(button=="mouse6"){if(checked){this.setState({mice_BLUE2: 1});}else{this.setState({mice_BLUE2: 0});}}
if(button=="mouse7"){if(checked){this.setState({mice_GREEN2: 1});}else{this.setState({mice_GREEN2: 0});}}
if(button=="mouse8"){if(checked){this.setState({mice_ORANGE2: 1});}else{this.setState({mice_ORANGE2: 0});}}

}

quick(e){

this.setState({processing: 'Please Wait...', waiting: '', complete: ''});
var all_rows=[];
var error_code=0;
var yyLabels = new Array(6).fill(0).map((_, i) => `${i}`);
	var xxLabels = new Array(13).fill(0).map((_, i) => `${i}`);
	var ddata = _.range(6).map(function () {
        // Create one row
        return _.range(13).map(function () {
            return parseFloat("0");//Math.floor(Math.random() * 100);
        });
    });
var all_time = moment("12-12-1970","YYYY-MM-DD");
var valid_mice_rows=[];

if(this.state.file_select=="1"){all_rows=global.CSV1;}
else if(this.state.file_select=="2"){all_rows=global.CSV2;}
else if(this.state.file_select=="3"){all_rows=global.CSV3;}
else if(this.state.file_select=="4"){all_rows=global.CSV4;}
else if(this.state.file_select=="5"){all_rows=global.CSV5;}
else {all_rows=null;}

if((!this.state.mice_RED && !this.state.mice_BLUE && !this.state.mice_GREEN && !this.state.mice_ORANGE && !this.state.mice_RED2 && !this.state.mice_BLUE2 && !this.state.mice_GREEN2 && !this.state.mice_ORANGE2)){error_code=1;}

if(all_rows==null){error_code=2;}

if(this.state.time_end.isBefore(this.state.time_start)){error_code=3;}
 
if(error_code!=0){
	if(error_code==1){this.setState({y_labels: [],x_labels: [],datapoints: [],csvdata_mice_rows:[],processing: '', waiting: 'error: No Mice Selected', complete: ''});}
	if(error_code==2){this.setState({y_labels: [],x_labels: [],datapoints: [],csvdata_mice_rows:[],processing: '', waiting: 'error: Empty CSV/No CSV Selected', complete: ''});}
	if(error_code==3){this.setState({y_labels: [],x_labels: [],datapoints: [],csvdata_mice_rows:[],processing: '', waiting: 'error: Start/End time', complete: ''});}
}
else{

	
for(var i = 0; i < all_rows.length; i++){
	var unit_label = all_rows[i][2];
	//purge bad mice
	if(unit_label=="unknown"||
	unit_label==" unit_label"||
	(!this.state.mice_RED && unit_label=="RED")||
	(!this.state.mice_BLUE && unit_label=="BLUE")||
	(!this.state.mice_GREEN && unit_label=="GREEN")||
	(!this.state.mice_ORANGE && unit_label=="ORANGE")||
	(!this.state.mice_RED2 && unit_label=="RED2")||
	(!this.state.mice_BLUE2 && unit_label=="BLUE2")||
	(!this.state.mice_GREEN2 && unit_label=="GREEN2")||
	(!this.state.mice_ORANGE2 && unit_label=="ORANGE2")){continue;}
	var datetime = moment(String(all_rows[i][0]).slice(0,19));
	//purge bad time
	if(this.state.time_start != all_time && (datetime < this.state.time_start || datetime > this.state.time_end)){continue;}
	var sector = all_rows[i][3];
	var duration = all_rows[i][4];
	valid_mice_rows.push(all_rows[i]);
		      //ddata[y][x]
	if(sector=="1"){ddata[1][12]+=parseFloat(duration)*parseFloat(".002");}
	if(sector=="2"){ddata[3][12]+=parseFloat(duration)*parseFloat(".002");}
	if(sector=="3"){ddata[2][11]+=parseFloat(duration)*parseFloat(".002");}
	if(sector=="4"){ddata[4][11]+=parseFloat(duration)*parseFloat(".002");}
	if(sector=="5"){ddata[1][10]+=parseFloat(duration)*parseFloat(".002");}
	if(sector=="6"){ddata[3][10]+=parseFloat(duration)*parseFloat(".002");}
	if(sector=="7"){ddata[2][9]+=parseFloat(duration)*parseFloat(".002");}
	if(sector=="8"){ddata[4][9]+=parseFloat(duration)*parseFloat(".002");}
	if(sector=="9"){ddata[1][8]+=parseFloat(duration)*parseFloat(".002");}
	if(sector=="10"){ddata[3][8]+=parseFloat(duration)*parseFloat(".002");}
	if(sector=="11"){ddata[2][7]+=parseFloat(duration)*parseFloat(".002");}
	if(sector=="12"){ddata[4][7]+=parseFloat(duration)*parseFloat(".002");}
	if(sector=="13"){ddata[1][6]+=parseFloat(duration)*parseFloat(".002");}
	if(sector=="14"){ddata[3][6]+=parseFloat(duration)*parseFloat(".002");}
	if(sector=="15"){ddata[2][5]+=parseFloat(duration)*parseFloat(".002");}
	if(sector=="16"){ddata[4][5]+=parseFloat(duration)*parseFloat(".002");}
	if(sector=="17"){ddata[1][4]+=parseFloat(duration)*parseFloat(".002");}
	if(sector=="18"){ddata[3][4]+=parseFloat(duration)*parseFloat(".002");}
	if(sector=="19"){ddata[2][3]+=parseFloat(duration)*parseFloat(".002");}
	if(sector=="20"){ddata[4][3]+=parseFloat(duration)*parseFloat(".002");}
	if(sector=="21"){ddata[1][2]+=parseFloat(duration)*parseFloat(".002");}
	if(sector=="22"){ddata[3][2]+=parseFloat(duration)*parseFloat(".002");}
	if(sector=="23"){ddata[2][1]+=parseFloat(duration)*parseFloat(".002");}
	if(sector=="24"){ddata[4][1]+=parseFloat(duration)*parseFloat(".002");}
	if(sector=="30"){ddata[3][0]+=parseFloat(duration)*parseFloat(".002");}
	if(sector=="31"){ddata[5][3]+=parseFloat(duration)*parseFloat(".002");}
	if(sector=="32"){ddata[0][3]+=parseFloat(duration)*parseFloat(".002");}
}

this.setState({y_labels: yyLabels,x_labels: xxLabels,datapoints: ddata,csvdata_mice_rows:valid_mice_rows,processing: '', waiting: '', complete: 'DONE!'});
}

}




//*-----------------------------------------------
render() {
const format = 'YYYY-MM-DD HH:mm:ss';
const now = moment();
function getFormat(time) {
return time ? format : 'YYYY-MM-DD';
}
const defaultCalendarValue = now.clone();
defaultCalendarValue.add(-1, 'month');
const divStyle = {width: '800px'};
const timePickerElement = <TimePickerPanel defaultValue={moment('00:00:00', 'HH:mm:ss')} />;

const state = this.state;
const calendar_start = (<Calendar
style={{ zIndex: 1000 }}
formatter={getFormat(true)}
dateInputPlaceholder="please input"
timePicker={timePickerElement}
defaultValue={this.props.defaultCalendarValue}
showToday

/>);

const calendar_end = (<Calendar
style={{ zIndex: 1000 }}
formatter={getFormat(true)}
dateInputPlaceholder="please input"
timePicker={timePickerElement}
defaultValue={this.props.defaultCalendarValue}
showToday
/>);

var topspace = {
  padding: '50px 0px 50px'
};
var centerdivs = {float:'left'};
var homecontainer = {height: '1000px'};

return (	
<div className='home-container' style={homecontainer}>
<p>The Speed for this function depends on the computer, and parameters</p>
<p>Larger (250MB+) files may take up to 5+ minutes to render</p>
<div>
    <h3 className = "title" > Choose CSV File: </h3>
</div>
<div>
<form>
<label>csv 1<input name = "file1" type = "radio" value="1" checked={this.state.file_select === "1"} onChange={this.radioChange}/></label>
<label>csv 2<input name = "file2" type = "radio" value="2" checked={this.state.file_select === "2"} onChange={this.radioChange}/></label>
<label>csv 3<input name = "file3" type = "radio" value="3" checked={this.state.file_select === "3"} onChange={this.radioChange}/></label>
<label>csv 4<input name = "file4" type = "radio" value="4" checked={this.state.file_select === "4"} onChange={this.radioChange}/></label>
<label>csv 5<input name = "file5" type = "radio" value="5" checked={this.state.file_select === "5"} onChange={this.radioChange}/></label>
</form>
</div>
{/*------------------------------date-time inputs--------------------------------*/}

<div style={topspace}>
	<p>(leave times blank to select all)</p>
	<div >
	<div style={centerdivs}><h3>Start Time:</h3></div>
	<div style={centerdivs}><DatePicker
                    animation="slide-up"
                    //value={this.state.time_start}
                    disabled={false}
                    calendar={calendar_start}
                    onChange={newTime => this.setState({time_start: newTime})}
                >{
                    ({value}) => {
                        return (
                            <input value={value ? value.format('YYYY-MM-DD HH:mm:ss') : ''}/>
                        )
                    }
                }</DatePicker></div>
	</div>
	<div>
	<div style={centerdivs}><h3>End Time:</h3></div>
	<div style={centerdivs}><DatePicker
                    animation="slide-up"
                    //value={this.state.time_end}
                    disabled={false}
                    calendar={calendar_end}
                    onChange={newTime => this.setState({time_end: newTime})}
                >{
                    ({value}) => {
                        return (
                            <input value={value ? value.format('YYYY-MM-DD HH:mm:ss') : ''}/>
                        )
                    }
                }</DatePicker></div>
	</div>
</div>

{/*------------------------------mice select input--------------------------------*/}
<h3>Select Mice:</h3>

<label><input name = "mouse9" type = "checkbox" onChange={this.onChange2}/>ALL</label>
<label><input name = "mouse1" type = "checkbox" onChange={this.onChange2} checked={this.state.mice_RED}/>RED</label>
<label><input name = "mouse2" type = "checkbox" onChange={this.onChange2} checked={this.state.mice_BLUE}/>BLUE</label>
<label><input name = "mouse3" type = "checkbox" onChange={this.onChange2} checked={this.state.mice_GREEN}/>GREEN</label>
<label><input name = "mouse4" type = "checkbox" onChange={this.onChange2} checked={this.state.mice_ORANGE}/>ORANGE</label>
<label><input name = "mouse5" type = "checkbox" onChange={this.onChange2} checked={this.state.mice_RED2}/>RED2</label>
<label><input name = "mouse6" type = "checkbox" onChange={this.onChange2} checked={this.state.mice_BLUE2}/>BLUE2</label>
<label><input name = "mouse7" type = "checkbox" onChange={this.onChange2} checked={this.state.mice_GREEN2}/>GREEN2</label>
<label><input name = "mouse8" type = "checkbox" onChange={this.onChange2} checked={this.state.mice_ORANGE2}/>ORANGE2</label>


{/*------------------------------submit-button--------------------------------*/}
<button style={divStyle} onClick = {this.quick}>submit</button>

{/*------------------------------heat-map--------------------------------*/}


    <h2 className = "title" > Status:</h2>
    <h3 className = "waiting" >{this.state.waiting}</h3>
    <h3 className = "processing" >{this.state.processing}</h3>
    <h3 className = "complete" >{this.state.complete}</h3>
<div style={divStyle}>

<div>
<HeatMap
xLabels={this.state.x_labels}
yLabels={this.state.y_labels}
data={this.state.datapoints}
onClick={(x, y) => (x==12 && y==1) ? alert(`Sector 1`):(x==12 && y==3) ? alert(`Sector 2`):(x==11 && y==2) ? alert(`Sector 3`):(x==11 && y==4) ? alert(`Sector 4`):(x==10 && y==1) ? alert(`Sector 5`):(x==10 && y==3) ? alert(`Sector 6`):(x==9 && y==2) ? alert(`Sector 7`):(x==9 && y==4) ? alert(`Sector 8`):(x==8 && y==1) ? alert(`Sector 9`):(x==8 && y==3) ? alert(`Sector 10`):(x==7 && y==2) ? alert(`Sector 11`):(x==7 && y==4) ? alert(`Sector 12`):(x==6 && y==1) ? alert(`Sector 13`):(x==6 && y==3) ? alert(`Sector 14`):(x==5 && y==2) ? alert(`Sector 15`):(x==5 && y==4) ? alert(`Sector 16`):(x==4 && y==1) ? alert(`Sector 17`):(x==4 && y==3) ? alert(`Sector 18`):(x==3 && y==2) ? alert(`Sector 19`):(x==3 && y==4) ? alert(`Sector 20`):(x==2 && y==1) ? alert(`Sector 21`):(x==2 && y==3) ? alert(`Sector 22`):(x==1 && y==2) ? alert(`Sector 23`):(x==1 && y==4) ? alert(`Sector 24`):(x==0 && y==3) ? alert(`Sector 30`):(x==3 && y==5) ? alert(`Sector 31`):(x==3 && y==0) ? alert(`Sector 32`):x }
/>
</div>





</div>
{/*-----------------------------testing purposes---------------------------*/}

{/*
<div>{this.test_print()}</div>
<div>{this.state.file_select}</div>
<div>{this.state.time_start.format('YYYY-MM-DD HH:mm:ss')}</div>
<div>{this.state.csvdata_mice_rows}</div>
<div>{this.state.time_start.to()}</div>
<div>{this.state.x_labels}</div>
<div>{this.state.mice_1}{this.state.mice_2}{this.state.mice_3}{this.state.mice_4}{this.state.mice_5}{this.state.mice_6}</div>
<div>{global.CSV1}</div>
<div>{this.msToTime()}</div>
<div>{this.test_print()}</div>
<div>{this.state.random_num}</div>
<button style={divStyle} onClick = {this.quick}>quick</button>
<div>{this.state.mice_RED}{this.state.mice_BLUE}{this.state.mice_GREEN}{this.state.mice_ORANGE}{this.state.mice_RED2}{this.state.mice_BLUE2}{this.state.mice_GREEN2}{this.state.mice_ORANGE2}</div>

<p>hghh</p>
*/}
{/*==================dont change below====================================*/}
</div>
); 
}
}
export default Heatmap;

