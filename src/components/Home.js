var React = require('react');
var Link = require('react-router-dom').Link;
var NavLink = require('react-router-dom').NavLink;


class Home extends React.Component {
  render() {
    return (
      <div className='home-container'>

        <h1>Mice Analysis</h1>
        <p class="break"> This is an application  for CPSC430. </p>
        <p2> Scott Summerford: Website Design, User Accounts, Layout, Analysis(Temp), and Backend</p2>
        <p3> Alfredo Soto    : Heat Map, Slidebar, and Display options </p3>
        <p4 class ="break"> Brian Walsh     : Vector Map, Grid Layout, RFID Functions </p4>
        <p5 class = "break"> The intent of this project is to build off the information from group 1, and provide the following functions: </p5>

      <ol class = "a">
        <li>A Heat Map of mice activity</li>
        <li>A Vector Map of mice movement</li>
        <li>A Predictive Analysis of mice activity</li>
        <li>Data Entry and Formatting</li>
        <li class = "break">User Registration</li>
      </ol> 

      <p class = "break"> Some functions may not be available in this iteration of the project. </p>
      <h1>Hosting Information </h1>
      <p class = "break"> All code is hosted on a S3 bucket on EC2-East for reliable ping, easy migration, and low maintenance cost. </p>
      <p2 class = "break"> The server-side code is composed of an optimized React build, which allows for public access while maintaining code privacy. </p2>
      <p2> Each function and corresponding webpage is segmented, and able to be modified without alteration of other methods. </p2>


      </div>
    )
  }
}

module.exports = Home;