
var Analysis = require('./Analysis');
var React = require('react');
require("./Login.css");
var NavLink = require('react-router-dom').NavLink;

var names = ['username'];
var passwords = ['password'];

/**This class cheeses out not having a database by using a static array.
* In future iterations, modify code to use databases.
* Similarly, there may be no need to have a registration function in the future.
**/
class Login extends React.Component {

  constructor() {
    super();

    this.state = {
      username: '',
      password: '',
      error: '',
      validname: 'username',
      validpassword: 'password',
      validation: '',
    };

    this.handlePassChange = this.handlePassChange.bind(this);
    this.handleUserChange = this.handleUserChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleRegister = this.handleRegister.bind(this);
    this.dismissError = this.dismissError.bind(this);
  }

  dismissError() {
    this.setState({ error: '' });
  }

  handleSubmit(evt) {
    evt.preventDefault();

    if (!this.state.username) {
      this.setState({ validation: '' });	
      return this.setState({ error: 'Login: Username is required' });

    }else if (!this.state.password) {
   	  this.setState({ validation: '' });	
      return this.setState({ error: 'Login: Password is required' });

    }else{
    	var valid_pair = 0;
    	var i = 0;
    	for (i = 0; i < names.length; i++) {
		    if(names[i] == this.state.username && passwords[i] == this.state.password){
		    	valid_pair = 1;
		    }
		}

		if(valid_pair == 0){
			this.setState({ validation: '' });
			return this.setState({error: 'Login failed!'});
		}else{
			names.push(this.state.username);
			passwords.push(this.state.password);
			this.setState({ error: '' });
      this.setState({validation: 'Login Successful! Redirecting ...'});
      global.USERLOGGEDIN = 1;
      this.props.history.push("/Upload");

      return this.setState({validation: 'Login Successful! Redirecting ...'});

		}

    }
    //Clears error status
    this.setState({ validation: '' });
    return this.setState({ error: 'Login invalid!' });
  }

  handleRegister(evt) {
    evt.preventDefault();

    if (!this.state.username) {
      return this.setState({ error: 'Registration: Username is required' });
    }else if (!this.state.password) {
      return this.setState({ error: 'Registration: Password is required' });
    }else{
    	var valid_pair = 1;
    	var i = 0;
    	for (i = 0; i < names.length; i++) {
		    if(names[i] == this.state.username){
		    	valid_pair = 0;
		    }
		}

		if(valid_pair == 0){
			this.setState({ validation: '' });
			return this.setState({error: 'User is already registered!'});
		}else{
			names.push(this.state.username);
			passwords.push(this.state.password);
			this.setState({ error: '' });
			return this.setState({validation: 'User Registered!'});
		}
    }

    //Clears error status.
    return this.setState({ error: '' });
  }

  handleUserChange(evt) {
    this.setState({
      username: evt.target.value,
    });
  };

  handlePassChange(evt) {
    this.setState({
      password: evt.target.value,
    });
  }

  render() {
    // NOTE: I use data-attributes for easier E2E testing
    // but you don't need to target those (any css-selector will work)

    if(global.USERLOGGEDIN){
          this.props.history.push("/Upload");
    }

    return (
      <div className="Login">
        <form onSubmit={this.handleSubmit}>

          
          <h3 className = "title"> User Login </h3>
          <label>User Name: </label>
          <input type="text" data-test="username" value={this.state.username} onChange={this.handleUserChange} />

          <label>Password: </label>
          <input type="password" data-test="password" value={this.state.password} onChange={this.handlePassChange} />

          <input type="submit" value="Log In" data-test="submit" />
          <input type="button" value="Register" data-test="register" onClick ={this.handleRegister}/>


          {
            this.state.error &&
            <h3 data-test="error" onClick={this.dismissError}>
              <button onClick={this.dismissError}>✖</button>
              {this.state.error}
            </h3>
          }


          {
            this.state.validation &&
            <h3 data-test="validation" onClick={this.dismissError}>
              <button onClick={this.dismissError}>✖</button>
              {this.state.validation}
            </h3>
          }

        </form>
      </div>
    );
  }
}

module.exports = Login;