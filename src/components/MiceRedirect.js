
var React = require('react');
require("./Login.css");
var NavLink = require('react-router-dom').NavLink;


/**This class cheeses out not having a database by using a static array.
* In future iterations, modify code to use databases.
* Similarly, there may be no need to have a registration function in the future.
**/
class MiceRedirect extends React.Component {

  constructor() {
    super();

    this.state = {
      username: '',
      password: '',
      error: '',
      validname: 'username',
      validpassword: 'password',
      validation: '',
    };
  }

  render() {

    return (
      <div className="Login">
        <h1> Redirecting to Mice 1 Website </h1>
      </div>
    );
  }
}

module.exports = MiceRedirect;