var React = require('react');
var Link = require('react-router-dom').Link;

//TODO: check input for validity based on dataset and common sense
//TODO: for mouse in query.rows, checkbox. Each checked box adds mouse to array
//TODO: generate vector map from data and given parameters
//TODO: create player for vector map
class Vectormap extends React.Component {
  
	constructor (props) {
		super(props);
		this.state = {
			startdate: '',
			starttime: '',
			enddate: '',
			endtime: ''
		};
		this.handleChange = this.handleChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}
  
	handleChange (event) {
		
		/*
		const target = event.target;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const name = target.name;
		*/
		
		this.setState({
			[event.target.name]: event.target.value 
		});
	}
  
	handleSubmit (event){
		alert('A time frame was submitted: \n' + this.state.starttime + ' on ' + this.state.startdate + ' to ' + this.state.endtime + ' on ' + this.state.enddate);
	}
  
	render () {
		return (
		
			<div className='home-container'>
				<h1> A First Look at the Vector Map Interface</h1>
				
				<img 
					src="https://i.gyazo.com/6de4c8a64e923acb85461bb028a1658b.png"
					alt="Failed to load Vector Map image"
					height = "200"
					width = "300"
					align = "right"
					border = "10"
					border = "white"
				/>
				
				<form onSubmit = {this.handleSubmit}>
					<label>
						Start Date
						<input 
							type="text" 
							name = "startdate" 
							value={this.state.startdate.value}
							onChange={this.handleChange}
						/>
					</label>
						
					<label> Start Time
						<input 
							type="text" 
							name="starttime" 
							value={this.state.starttime.value}
							onChange={this.handleChange}
						/>
					</label>
					
					<br/>
					<br/>	
					
					<label> End Date
						<input 
							type="text" 
							name="enddate" 
							value={this.state.enddate.value}
							onChange={this.handleChange}
						/>
					</label>
						
					<label> End Time
						<input 
							type="text" 
							name="endtime" 
							value={this.state.endtime.value}
							onChange={this.handleChange}
						/>
					</label>
					
					<br/>
					<br/>
					
					<label> Mouse 1
						<input
							name = "mouse1"
							type = "checkbox"
							//value = {this.state.numberOfGuests}
							//onChange = {this.handleChange}
						/>
					</label>	
					
					<label> Mouse 2
						<input
							name = "mouse2"
							type = "checkbox"
							//value = {this.state.numberOfGuests}
							//onChange = {this.handleChange}
						/>
					</label>	
					
					<br/>
					
					<input 
						type="submit" 
						value = "Submit"
					/>
										
				</form>
			</div>
		);
	}
}

module.exports = Vectormap;
