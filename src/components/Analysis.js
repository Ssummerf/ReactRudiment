import Papa from 'papaparse';
import Select from 'react-select';
var React = require('react');
var Link = require('react-router-dom').Link;
var CsvToHtmlTable = require('react-csv-to-table').CsvToHtmlTable;

require('./Analysis.css');


var colors = [
  { value: 0, label: 'RED' },
  { value: 1, label: 'ORANGE' },
  { value: 2, label: 'BLUE' },
  { value: 3, label: 'GREEN' }
];

var CSVLIMIT = 5;
var nonbroken_select = colors[0];
var nonbroken_select2 = global.options[0];
var filename;

var csvString = '';
var csvArray = [];

class Analysis extends React.Component {
	
 constructor() {
    super();

    this.state = {
      tileprediction: 0,
      likelihood: 0,
      mouse: '',
      error: '',
      selectedOption: global.options[0],
      selectedColor: colors[0],
      arrayString: "",
      waiting: 'Waiting on input',
      processing: '',
      complete: '',
    };

    this.config = {
      delimiter: "",  // auto-detect
      newline: "",
      fastMode: true,
      worker: true
    };

    //Bind our functions
    this.testPapa = this.testPapa.bind(this);

    this.handlePredict = this.handlePredict.bind(this); 
    this.handleMouseChange = this.handleMouseChange.bind(this);
    this.dismissError = this.dismissError.bind(this);
    this.preformParse = this.preformParse.bind(this);
    this.handleColor = this.handleColor.bind(this);
    this.predictMovements = this.predictMovements.bind(this);
  }

  handleChange = (selectedOption) => {
    this.setState({ selectedOption });
    nonbroken_select2 = selectedOption;
    console.log(`Option selected:`, selectedOption);
  }

  handleColor = (selectedColor) => {
    this.setState({ selectedColor });
    nonbroken_select = selectedColor;
  }

  dismissError() {
    this.setState({ error: '' });
  }

  handlePredict(evt){
    console.log(global.csvArray1);
  	evt.preventDefault();
  	if(!this.state.mouse){
  		return this.setState({error: 'No mouse chosen!'});
  	}
  	this.setState({error: ''});
  	this.setState({likelihood: Math.floor((Math.random() * 99) + 1) });
  	return this.setState({tileprediction:Math.floor((Math.random() * 23) + 1) });

  }

  handleMouseChange(evt){
  	this.setState({
      mouse: evt.target.value,
    });
  }

    testPapa(e){
    //Make sure our global count exists.
    if(global.GLOBALCOUNT == null)
      global.GLOBALCOUNT = 1;

    //Reset our global count on any
    if(global.GLOBALCOUNT > CSVLIMIT)
      global.GLOBALCOUNT == 1;

    filename = e.target.files[0].name;

    csvString = '';
    this.setState({processing: 'WORKING - Please Wait!', waiting: '', complete: ''});
    //Build the Array Object before the String Object
    var csv2 = Papa.parse(e.target.files[0], {
      download: true,
      complete: function(results) {
        console.log("All done!");

        switch(nonbroken_select.value){
          case 0: global.CSV1 = results.data; break;
          case 1: global.CSV2 = results.data; break;
          case 2: global.CSV3 = results.data; break;
          case 3: global.CSV4 = results.data; break;
          case 4: global.CSV5 = results.data; break;
          default: global.CSV1 = results.data; break;
        }

        global.GLOBALCOUNT++;
        var newname = nonbroken_select2.label + ' (' + filename + ')';
        var oldkey = nonbroken_select2.label;

        global.options[nonbroken_select.value].label = newname;
        global.options[newname] = global.options[oldkey];
        delete global.options[oldkey];
        }
        });

    //Build the String object.
    var csv = Papa.parse(e.target.files[0], {
      download: true,
      step: function(row) {
        //Only continue adding to our preview if the size is reasonable.
        //Nobody's going to check every line of a 1GB file outside excel.
        if(csvString.length < 15000){
          var rowarray = row.data;

          //Concantenate into string for table conversion
          for(var i = 0; i < rowarray.length; i++){
            csvString = csvString + rowarray[i] + ',';
          }

          csvString = csvString.substring(0, csvString.length-1);
          //Add new line to segment rows
          csvString = csvString + '\n';
        }
      },
      complete: function(results) {
        console.log("All done!");
        //By calling setState, we force the table to refresh its contents
        //So long as the table pulls its data from state.
        this.setState({data: csvString});
        this.setState({complete: 'File has been parsed!', waiting: '', processing: ''});
      }.bind(this)  //THIS MUST BE BINDED OR ELSE IT CANNOT ACCESS THE STATE.
        });

  }

  predictMovements(){
    console.log("Switch case activated");

    var selectedTemp = nonbroken_select2;

        switch(selectedTemp.value){
          case 0: this.preformParse(global.CSV1); break;
          case 1: this.preformParse(global.CSV2); break;
          case 2: this.preformParse(global.CSV3); break;
          case 3: this.preformParse(global.CSV4); break;
          case 4: this.preformParse(global.CSV5); break;
          default: this.preformParse(global.CSV1); break;
        }
  }

  preformParse(CSVtoParse){
    this.setState({processing: 'WORKING - Please Wait!', waiting: '', complete: ''});
    console.log("Preform parse activated");
    var localCSV = CSVtoParse;
    var colorToCheck = nonbroken_select;

    //Make sure that the CSV is not null, and that its rows have at least 6 columns, or it will fail.
    var arrString = "Color: ," + colorToCheck.label + ", , " + "\n";

    //Build output array
    var arr2 = new Array(32);
    for (var i = 0; i < arr2.length; i++) {
      arr2[i] = 0;
    }

    var lastTime = 0;
    var currentTime = 0;
    var sumColumn = 1;

    //Must be non null, must have a length > 5, must have a semicolon splitting operator.
    if(localCSV != null){
      console.log("Loop activated");
      console.log(colorToCheck.label);
      for(var i = 0; i < CSVtoParse.length-1; i++){
        if(localCSV[i][2].localeCompare(colorToCheck.label)){
          var gridloc = parseInt(localCSV[i][3]);
          arr2[gridloc] = arr2[gridloc] + parseInt(localCSV[i][4]);

        }
      }
    }

    for(var i = 0; i < arr2.length-1; i++){
        arr2[i] = arr2[i] / 1000;

        var date = new Date(null);
        date.setSeconds(arr2[i]); // specify value for SECONDS here
        var result = date.toISOString().substr(14, 8);

        arrString = arrString + "Grid #: " + (i+1) + "," + result;
      

        arrString = arrString + "\n";
    }
    this.setState({complete: 'File has been parsed!', waiting: '', processing: ''});
    this.setState({arrayString: arrString});
  }

  render() {

    const { selectedOption } = this.state;
    const { selectedColor } = this.state;
    return (


      <div className='home-container'>
      <div className ='selections'>
        <h1> Parameters </h1>
        <p> Select a file or upload a new file to analyze </p>

    <Select
      value={selectedOption}
      onChange={this.handleChange}
      options={global.options}
    />

    <p> Select a mouse color to analyze </p>

    <Select
      value={selectedColor}
      onChange={this.handleColor}
      options={colors}
    />
    </div>

    <div className = 'Analyze'>
    <h1> Analyze Mouse Movements </h1>
     <button onClick={this.predictMovements}>Start</button>
     </div>

    <h3 className = "waiting" >{this.state.waiting}</h3>
    <h3 className = "processing" >{this.state.processing}</h3>
    <h3 className = "complete" >{this.state.complete}</h3>

       <div className = "htmltable">
      <CsvToHtmlTable
        data={this.state.arrayString}
        csvDelimiter=","
        tableClassName="table table-striped table-hover"
      />
      </div>
      </div>
    )
  }
}

export default Analysis;