
var React = require('react');
var NavLink = require('react-router-dom').NavLink;

function Nav () {
  return (
    <ul className='nav'>
      <li>
        <NavLink exact className="inactive" activeClassName='active' to='/'>Home</NavLink>
      </li>
      <li>
        <NavLink className="inactive" activeClassName='active' to='/heat-map'>Heat-Map</NavLink>
      </li>
      <li>
        <NavLink className="inactive" activeClassName='active' to='/Vector-Map'>Vector-Map</NavLink>
      </li>
      <li>
        <NavLink className="inactive" activeClassName='active' to='/Analysis'>Predictions</NavLink>
      </li>
      <li>
        <NavLink className="inactive" activeClassName='active' to='/Login'>Upload</NavLink>
      </li>
        <li>
        <NavLink className="inactive" activeClassName='active' to='/MiceRedirect'>Group 1 Analysis</NavLink>
      </li>
    </ul>
  )
}

module.exports = Nav;