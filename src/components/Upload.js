import Papa from 'papaparse';
import Select from 'react-select';
require("./Login.css");
var CsvToHtmlTable = require('react-csv-to-table').CsvToHtmlTable;
var React = require('react');
var Link = require('react-router-dom').Link;

//YOUR GLOBAL VARIABLES FOR FILE PERSISTENCE ARE AS FOLLOWS:
/**
*CSV1
*CSV2
*CSV3
*CSV4
*CSV5
*THE COUNT IS global.GLOBALCOUNT
*/

//Selection bar options
global.options = [
  { value: 0, label: 'CSV_Slot_1' },
  { value: 1, label: 'CSV_Slot_2' },
  { value: 2, label: 'CSV_Slot_3' },
  { value: 3, label: 'CSV_Slot_4' },
  { value: 4, label: 'CSV_Slot_5' }
];

/**
* So, there are several limitations and functions that React has.
* PapaParse instantiates its own unique functions, so we need global variables
* if any outside variable needs to work inside of it.
* Similarly, to pass variables to other file-functions that persist within our React program, use global.varname
**/

var CSVLIMIT = 5;
var nonbroken_select = global.options[0];
var filename;

var csvString = '';
var csvArray = [];

class Upload extends React.Component {

 constructor(){
  super();
  this.state = {
    csvData: '',
    data: '',
    waiting: 'Waiting on Upload',
    processing: '',
    complete: '',
    selectedOption: global.options[0]
  };

  this.config = {
    delimiter: "",  // auto-detect
    newline: "",
    fastMode: true,
    worker: true
  };

  //Bind our function
  this.testPapa = this.testPapa.bind(this);
}

  handleChange = (selectedOption) => {
    this.setState({ selectedOption });
    nonbroken_select = selectedOption;
    console.log(`Option selected:`, selectedOption);
  }


  testPapa(e){
    //Make sure our global count exists.
    if(global.GLOBALCOUNT == null)
      global.GLOBALCOUNT = 1;

    //Reset our global count on any
    if(global.GLOBALCOUNT > CSVLIMIT)
      global.GLOBALCOUNT == 1;

    filename = e.target.files[0].name;

    csvString = '';
    this.setState({processing: 'WORKING - Please Wait!', waiting: '', complete: ''});
    //Build the Array Object before the String Object
    var csv2 = Papa.parse(e.target.files[0], {
      download: true,
      complete: function(results) {
        console.log("All done!");

        switch(nonbroken_select.value){
          case 0: global.CSV1 = results.data; break;
          case 1: global.CSV2 = results.data; break;
          case 2: global.CSV3 = results.data; break;
          case 3: global.CSV4 = results.data; break;
          case 4: global.CSV5 = results.data; break;
          default: global.CSV1 = results.data; break;
        }

        global.GLOBALCOUNT++;
        var newname = nonbroken_select.label + ' (' + filename + ')';
        var oldkey = nonbroken_select.label;

        global.options[nonbroken_select.value].label = newname;
        global.options[newname] = global.options[oldkey];
        delete global.options[oldkey];
        }
        });

    //Build the String object.
    var csv = Papa.parse(e.target.files[0], {
      download: true,
      step: function(row) {
        //Only continue adding to our preview if the size is reasonable.
        //Nobody's going to check every line of a 1GB file outside excel.
        if(csvString.length < 15000){
          var rowarray = row.data;

          //Concantenate into string for table conversion
          for(var i = 0; i < rowarray.length; i++){
            csvString = csvString + rowarray[i] + ',';
          }

          csvString = csvString.substring(0, csvString.length-1);
          //Add new line to segment rows
          csvString = csvString + '\n';
        }
      },
      complete: function(results) {
        console.log("All done!");
        //By calling setState, we force the table to refresh its contents
        //So long as the table pulls its data from state.
        this.setState({data: csvString});
        this.setState({complete: 'File has been parsed!', waiting: '', processing: ''});
      }.bind(this)  //THIS MUST BE BINDED OR ELSE IT CANNOT ACCESS THE STATE.
        });

  }

  //Dismiss errors.
  dismissError() {
    this.setState({ error: '' });
  }

render() {

//Comment this for testing.
if(global.USERLOGGEDIN != 1){
  this.props.history.push("/Login");
}

const { selectedOption } = this.state;

return (

    <div className = "Upload">
    <p>The Speed of this function depends on your computer. </p>
    <p>Large (1GB+) files may take up to 10 minutes to parse. </p>
    <p>Once a file is uploaded, it is available to all other functions. </p>
    <h2 className = "title" > Upload File: </h2>
    <input type="file" onChange={this.testPapa } />
    <p>Select the slot to save to </p>
    <Select
      value={selectedOption}
      onChange={this.handleChange}
      options={global.options}
    />
    <h2 className = "title" > Status:</h2>
    <h3 className = "waiting" >{this.state.waiting}</h3>
    <h3 className = "processing" >{this.state.processing}</h3>
    <h3 className = "complete" >{this.state.complete}</h3>



    <div className = "htmltable">
    <CsvToHtmlTable
      data={this.state.data}
      csvDelimiter=","
      tableClassName="table table-striped table-hover"
    />
    </div>
   </div>

    );
  }
}

export default Upload;